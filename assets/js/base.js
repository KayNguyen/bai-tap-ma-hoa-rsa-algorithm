$(function () {


    // Giải mã, mã hoá
    $('#execute').click(function () {

        // Khởi tạo thư viện JSEncrypt
        var crypt = new JSEncrypt();

        // set private Key có sẵn
        crypt.setPrivateKey($('#private_key').val());
        // nếu Public key rỗng thì tạo mới theo private key
        var pubkey = $('#public_key').val();
        if (!pubkey) {
            $('#public_key').val(crypt.getPublicKey());
        }

        // Dữ liệu cần mã hoá
        var input = $('#input').val();
        // Dữ liệu đã mã hoá
        var crypted = $('#crypted').val();

        // Alternate the values.
        if (input) {
            try {
                let val_en = crypt.encrypt(input);
                console.log(val_en)
                $('#crypted').val(val_en);
                $('#input').val('');
                $('#execute').text('Giải mã');
            }catch (e) {
                console.log(e)
                $('#crypted').val(e);
            }

        } else if (crypted) {
            console.log(crypted)
            var decrypted = crypt.decrypt(crypted);
            console.log(decrypted)
            $('#crypted').val('');
            $('#execute').text('Mã hoá');
            $('#input').val(decrypted);


        }
    });

    var generateKeys = function () {
        var sKeySize = $('#key-size').val();
        var keySize = parseInt(sKeySize);
        var crypt = new JSEncrypt({default_key_size: keySize});
        var dt = new Date();
        var time = -(dt.getTime());
        crypt.getKey();
        dt = new Date();
        time += (dt.getTime());
        $('#time-report').text('Thời gian tạo: ' + time + ' ms');
        $('#private_key').val(crypt.getPrivateKey());
        $('#public_key').val(crypt.getPublicKey());
    };

    //Tạo khoá
    $('#generate').click(generateKeys);
    $('#example').click(function () {
        let de = new Date();
        let decrypted = '{"id":'+de.getTime()+',"name":"Nguyen Van Khoa","mobile":"0886509919","email":"khoait'+(Math.floor(Math.random() * 100) + 1)+'@gmail.com"}';
        $('#input').val(decrypted);
    });

    generateKeys();

});
